## вывод приемеов которые уже прошли

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using App;
using CORE.Controllers;
using CORE.Models;

namespace AppClinic
{
    public partial class PastPage : Page
    {
        private AppointmentController _appointmentController = new AppointmentController();
        private DoctorController _doctorController = new DoctorController();
        private PatientController _patientController = new PatientController();

        private List<AppointmentModel> _appointment
        {
            get
            {
                if (_appointmentController.GetAll().Count == 0)
                    return new List<AppointmentModel>();
                return _appointmentController.GetPast().OrderByDescending(a => a.Date).ToList();;
            }
        }

        public PastPage()
        {
            InitializeComponent();

            listBox.SelectionChanged += (sender, args) =>
            {
                if (!(sender is ListBox listBox1) || listBox1.SelectedItem == null) return;
                int tag = ((AppointmentModel)listBox1.SelectedItem).Id;

                var win = new MedicalHistory(tag);
                win.ShowDialog();
                listBox1.SelectedItem = null;

            };

            listBox.ItemsSource = _appointment;
        }
    }
}

```
