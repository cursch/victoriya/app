using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using CORE.Controllers;
using CORE.Models;
using Xceed.Wpf.Toolkit;
using MessageBox = System.Windows.MessageBox;

namespace AppClinic
{
    public partial class Appointment : Page
    {
        
        
        private AppointmentController _appointmentController = new AppointmentController();
        private DoctorController _doctorController = new DoctorController();
        private PatientController _patientController = new PatientController();

        private List<AppointmentModel> _appointment
        {
            get
            {
                return _appointmentController.GetFirst().OrderBy(a => a.Date).ToList();;
            }
        }

        public Appointment()
        {
            InitializeComponent();
            
            listBox.SelectionChanged += (sender, args) =>
            {
                if (!(sender is ListBox listBox1) || listBox1.SelectedItem == null) return;
                int tag = ((AppointmentModel)listBox1.SelectedItem).Id;

                MessageBoxResult result = MessageBox.Show("Вы уверены что хотите удалить элемент?",
                    "Подтверждение удаления", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    _appointmentController.Remove(tag);
                }
                else
                {
                    return;
                }

                listBox.Items.Refresh();
            };
            __ComboBoxDoctor.ItemsSource = _doctorController.GetAll();
            __ComboBoxPatient.ItemsSource = _patientController.GetAll();

            listBox.ItemsSource = _appointment;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (__ComboBoxPatient.SelectedValue == null ||__ComboBoxDoctor.SelectedValue == null )
            {
                MessageBox.Show("Пожалуйста заполните все поля");
                return;
            }


            if (string.IsNullOrEmpty(__ComboBoxDoctor.SelectedValue.ToString()) ||
                string.IsNullOrEmpty(__ComboBoxPatient.SelectedValue.ToString()) ||
                string.IsNullOrEmpty(__TextBoxReport.Text) || __TextBoxDate.Value == null)
            {
                MessageBox.Show("Пожалуйста заполните все поля");
                return;
            }


            var appointment = new AppointmentModel()
            {
                DoctorId = (int)__ComboBoxDoctor.SelectedValue,
                PatientId = (int)__ComboBoxPatient.SelectedValue,
                Report = __TextBoxReport.Text,
                Date = __TextBoxDate.Value.Value
            };
            _appointmentController.Create(appointment);
            __ComboBoxDoctor.SelectedValue = null;
            __ComboBoxPatient.SelectedValue = null;
            
            __TextBoxReport.Text = String.Empty;
            listBox.ItemsSource = _appointment;
            listBox.Items.Refresh();
        }


        private void __ComboBox_OnContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            _doctorController.Load();
            _patientController.Load();
            __ComboBoxDoctor.ItemsSource = _doctorController.GetAll();
            __ComboBoxPatient.ItemsSource = _patientController.GetAll();
        }
    }
}