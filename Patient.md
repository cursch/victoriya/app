## Класс для рабы с Пациентами

```csharp
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using CORE.Models;

namespace AppClinic
{
    /// <summary>
    /// Логика взаимодействия для Patient.xaml
    /// </summary>
    public partial class Patient : Page
    {

        private CORE.Controllers.PatientController _patientController = new CORE.Controllers.PatientController();

        private List<PatientModel> _patient
        {
            get { return _patientController.GetAll() ; }
        }

        public Patient()
        {
            InitializeComponent();

            listBox.SelectionChanged += (sender, args) =>
            {
                if (!(sender is ListBox listBox1) || listBox1.SelectedItem == null) return;
                int tag = ((PatientModel)listBox1.SelectedItem).Id;

                MessageBoxResult result = MessageBox.Show("Вы уверены что хотите удалить элемент?",
                    "Подтверждение удаления", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.Yes)
                {
                    _patientController.Remove(tag);
                }
                else
                {
                    return;
                }

                listBox.Items.Refresh();
            };

            listBox.ItemsSource = _patient;


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var name = __TextBoxName.Text;
            var surname = __TextBoxSurname.Text;
            var middleName = __TextBoxPatronymic.Text;
            var phone = __TextBoxPhone.Text;
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(surname) || string.IsNullOrEmpty(middleName) ||
                string.IsNullOrEmpty(phone))
            {
                MessageBox.Show("Пожалуйста заполните все поля");
                return;
            }

            var patient = new PatientModel()
            {
                FirstName = $"{name}",
                LastName = $"{surname}",
                MiddleName = $"{middleName}",
                PhoneNumber = $"{phone}"
            };

            _patientController.Create(patient);

            __TextBoxName.Text = String.Empty;
            __TextBoxSurname.Text = String.Empty;
            __TextBoxPatronymic.Text = String.Empty;
            __TextBoxPhone.Text = String.Empty;

            listBox.Items.Refresh();
        }
    }
}

```
